// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/matth/OneDrive/Dokumente/GitHub/Test/frastanzerplanner/conf/routes
// @DATE:Fri Dec 18 13:21:28 CET 2020

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseUserdateController UserdateController = new controllers.ReverseUserdateController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseUserController UserController = new controllers.ReverseUserController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseDateController DateController = new controllers.ReverseDateController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseTimeController TimeController = new controllers.ReverseTimeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseFrontendController FrontendController = new controllers.ReverseFrontendController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseUserdateController UserdateController = new controllers.javascript.ReverseUserdateController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseUserController UserController = new controllers.javascript.ReverseUserController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseDateController DateController = new controllers.javascript.ReverseDateController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseTimeController TimeController = new controllers.javascript.ReverseTimeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseFrontendController FrontendController = new controllers.javascript.ReverseFrontendController(RoutesPrefix.byNamePrefix());
  }

}
