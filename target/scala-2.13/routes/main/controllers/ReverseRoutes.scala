// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/matth/OneDrive/Dokumente/GitHub/Test/frastanzerplanner/conf/routes
// @DATE:Fri Dec 18 13:21:28 CET 2020

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:8
package controllers {

  // @LINE:31
  class ReverseUserdateController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:40
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/applications/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:34
    def getUsers(id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/applications/event/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:38
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/applications/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:31
    def event(q:String = null): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/applications" + play.core.routing.queryString(List(if(q == null) None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("q", q)))))
    }
  
    // @LINE:32
    def get(id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/applications/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:36
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/applications")
    }
  
  }

  // @LINE:21
  class ReverseUserController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/users/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:21
    def user(q:String = null): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/users" + play.core.routing.queryString(List(if(q == null) None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("q", q)))))
    }
  
    // @LINE:22
    def get(id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/users/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:26
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/users")
    }
  
    // @LINE:24
    def login(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/users/login")
    }
  
  }

  // @LINE:11
  class ReverseDateController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:18
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/events/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:11
    def dates(q:String = null): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/events" + play.core.routing.queryString(List(if(q == null) None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("q", q)))))
    }
  
    // @LINE:16
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/events/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:12
    def get(id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/events/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:14
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/events")
    }
  
  }

  // @LINE:43
  class ReverseTimeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:44
    def get(id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/time/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:43
    def time(q:String = null): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/time" + play.core.routing.queryString(List(if(q == null) None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("q", q)))))
    }
  
    // @LINE:48
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/time/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:46
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/time")
    }
  
  }

  // @LINE:8
  class ReverseFrontendController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:51
    def assetOrDefault(file:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + implicitly[play.api.mvc.PathBindable[String]].unbind("file", file))
    }
  
    // @LINE:8
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }


}
