// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/matth/OneDrive/Dokumente/GitHub/Test/frastanzerplanner/conf/routes
// @DATE:Fri Dec 18 13:21:28 CET 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
