// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/matth/OneDrive/Dokumente/GitHub/Test/frastanzerplanner/conf/routes
// @DATE:Fri Dec 18 13:21:28 CET 2020

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:8
  FrontendController_0: controllers.FrontendController,
  // @LINE:11
  DateController_2: controllers.DateController,
  // @LINE:21
  UserController_4: controllers.UserController,
  // @LINE:31
  UserdateController_1: controllers.UserdateController,
  // @LINE:43
  TimeController_3: controllers.TimeController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:8
    FrontendController_0: controllers.FrontendController,
    // @LINE:11
    DateController_2: controllers.DateController,
    // @LINE:21
    UserController_4: controllers.UserController,
    // @LINE:31
    UserdateController_1: controllers.UserdateController,
    // @LINE:43
    TimeController_3: controllers.TimeController
  ) = this(errorHandler, FrontendController_0, DateController_2, UserController_4, UserdateController_1, TimeController_3, "/")

  def withPrefix(addPrefix: String): Routes = {
    val prefix = play.api.routing.Router.concatPrefix(addPrefix, this.prefix)
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, FrontendController_0, DateController_2, UserController_4, UserdateController_1, TimeController_3, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.FrontendController.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/events""", """controllers.DateController.dates(q:String ?= null)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/events/""" + "$" + """id<[^/]+>""", """controllers.DateController.get(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/events""", """controllers.DateController.add(request:Request)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/events/""" + "$" + """id<[^/]+>""", """controllers.DateController.update(request:Request, id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/events/""" + "$" + """id<[^/]+>""", """controllers.DateController.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/users""", """controllers.UserController.user(q:String ?= null)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/users/""" + "$" + """id<[^/]+>""", """controllers.UserController.get(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/users/login""", """controllers.UserController.login(request:Request)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/users""", """controllers.UserController.add(request:Request)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/users/""" + "$" + """id<[^/]+>""", """controllers.UserController.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/applications""", """controllers.UserdateController.event(q:String ?= null)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/applications/""" + "$" + """id<[^/]+>""", """controllers.UserdateController.get(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/applications/event/""" + "$" + """id<[^/]+>""", """controllers.UserdateController.getUsers(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/applications""", """controllers.UserdateController.add(request:Request)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/applications/""" + "$" + """id<[^/]+>""", """controllers.UserdateController.update(request:Request, id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/applications/""" + "$" + """id<[^/]+>""", """controllers.UserdateController.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/time""", """controllers.TimeController.time(q:String ?= null)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/time/""" + "$" + """id<[^/]+>""", """controllers.TimeController.get(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/time""", """controllers.TimeController.add(request:Request)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/time/""" + "$" + """id<[^/]+>""", """controllers.TimeController.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """""" + "$" + """file<.+>""", """controllers.FrontendController.assetOrDefault(file:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:8
  private[this] lazy val controllers_FrontendController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_FrontendController_index0_invoker = createInvoker(
    FrontendController_0.index(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FrontendController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ Serve index page from public directory""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_DateController_dates1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/events")))
  )
  private[this] lazy val controllers_DateController_dates1_invoker = createInvoker(
    DateController_2.dates(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DateController",
      "dates",
      Seq(classOf[String]),
      "GET",
      this.prefix + """api/events""",
      """ Events""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_DateController_get2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/events/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DateController_get2_invoker = createInvoker(
    DateController_2.get(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DateController",
      "get",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/events/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_DateController_add3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/events")))
  )
  private[this] lazy val controllers_DateController_add3_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      DateController_2.add(fakeValue[play.mvc.Http.Request]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DateController",
      "add",
      Seq(classOf[play.mvc.Http.Request]),
      "POST",
      this.prefix + """api/events""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:16
  private[this] lazy val controllers_DateController_update4_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/events/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DateController_update4_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      DateController_2.update(fakeValue[play.mvc.Http.Request], fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DateController",
      "update",
      Seq(classOf[play.mvc.Http.Request], classOf[Long]),
      "PUT",
      this.prefix + """api/events/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:18
  private[this] lazy val controllers_DateController_delete5_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/events/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DateController_delete5_invoker = createInvoker(
    DateController_2.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DateController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/events/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:21
  private[this] lazy val controllers_UserController_user6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/users")))
  )
  private[this] lazy val controllers_UserController_user6_invoker = createInvoker(
    UserController_4.user(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "user",
      Seq(classOf[String]),
      "GET",
      this.prefix + """api/users""",
      """ Users""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_UserController_get7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserController_get7_invoker = createInvoker(
    UserController_4.get(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "get",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/users/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_UserController_login8_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/users/login")))
  )
  private[this] lazy val controllers_UserController_login8_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      UserController_4.login(fakeValue[play.mvc.Http.Request]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "login",
      Seq(classOf[play.mvc.Http.Request]),
      "POST",
      this.prefix + """api/users/login""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:26
  private[this] lazy val controllers_UserController_add9_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/users")))
  )
  private[this] lazy val controllers_UserController_add9_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      UserController_4.add(fakeValue[play.mvc.Http.Request]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "add",
      Seq(classOf[play.mvc.Http.Request]),
      "POST",
      this.prefix + """api/users""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:28
  private[this] lazy val controllers_UserController_delete10_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserController_delete10_invoker = createInvoker(
    UserController_4.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/users/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:31
  private[this] lazy val controllers_UserdateController_event11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/applications")))
  )
  private[this] lazy val controllers_UserdateController_event11_invoker = createInvoker(
    UserdateController_1.event(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserdateController",
      "event",
      Seq(classOf[String]),
      "GET",
      this.prefix + """api/applications""",
      """Applications""",
      Seq()
    )
  )

  // @LINE:32
  private[this] lazy val controllers_UserdateController_get12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/applications/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserdateController_get12_invoker = createInvoker(
    UserdateController_1.get(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserdateController",
      "get",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/applications/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:34
  private[this] lazy val controllers_UserdateController_getUsers13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/applications/event/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserdateController_getUsers13_invoker = createInvoker(
    UserdateController_1.getUsers(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserdateController",
      "getUsers",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/applications/event/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:36
  private[this] lazy val controllers_UserdateController_add14_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/applications")))
  )
  private[this] lazy val controllers_UserdateController_add14_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      UserdateController_1.add(fakeValue[play.mvc.Http.Request]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserdateController",
      "add",
      Seq(classOf[play.mvc.Http.Request]),
      "POST",
      this.prefix + """api/applications""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:38
  private[this] lazy val controllers_UserdateController_update15_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/applications/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserdateController_update15_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      UserdateController_1.update(fakeValue[play.mvc.Http.Request], fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserdateController",
      "update",
      Seq(classOf[play.mvc.Http.Request], classOf[Long]),
      "PUT",
      this.prefix + """api/applications/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:40
  private[this] lazy val controllers_UserdateController_delete16_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/applications/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserdateController_delete16_invoker = createInvoker(
    UserdateController_1.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserdateController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/applications/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:43
  private[this] lazy val controllers_TimeController_time17_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/time")))
  )
  private[this] lazy val controllers_TimeController_time17_invoker = createInvoker(
    TimeController_3.time(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TimeController",
      "time",
      Seq(classOf[String]),
      "GET",
      this.prefix + """api/time""",
      """ Time (report)""",
      Seq()
    )
  )

  // @LINE:44
  private[this] lazy val controllers_TimeController_get18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/time/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_TimeController_get18_invoker = createInvoker(
    TimeController_3.get(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TimeController",
      "get",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/time/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:46
  private[this] lazy val controllers_TimeController_add19_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/time")))
  )
  private[this] lazy val controllers_TimeController_add19_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      TimeController_3.add(fakeValue[play.mvc.Http.Request]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TimeController",
      "add",
      Seq(classOf[play.mvc.Http.Request]),
      "POST",
      this.prefix + """api/time""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:48
  private[this] lazy val controllers_TimeController_delete20_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/time/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_TimeController_delete20_invoker = createInvoker(
    TimeController_3.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TimeController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/time/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:51
  private[this] lazy val controllers_FrontendController_assetOrDefault21_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_FrontendController_assetOrDefault21_invoker = createInvoker(
    FrontendController_0.assetOrDefault(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FrontendController",
      "assetOrDefault",
      Seq(classOf[String]),
      "GET",
      this.prefix + """""" + "$" + """file<.+>""",
      """ Serve static assets under public directory""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:8
    case controllers_FrontendController_index0_route(params@_) =>
      call { 
        controllers_FrontendController_index0_invoker.call(FrontendController_0.index())
      }
  
    // @LINE:11
    case controllers_DateController_dates1_route(params@_) =>
      call(params.fromQuery[String]("q", Some(null))) { (q) =>
        controllers_DateController_dates1_invoker.call(DateController_2.dates(q))
      }
  
    // @LINE:12
    case controllers_DateController_get2_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_DateController_get2_invoker.call(DateController_2.get(id))
      }
  
    // @LINE:14
    case controllers_DateController_add3_route(params@_) =>
      call { 
        controllers_DateController_add3_invoker.call(
          req => DateController_2.add(req))
      }
  
    // @LINE:16
    case controllers_DateController_update4_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_DateController_update4_invoker.call(
          req => DateController_2.update(req, id))
      }
  
    // @LINE:18
    case controllers_DateController_delete5_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_DateController_delete5_invoker.call(DateController_2.delete(id))
      }
  
    // @LINE:21
    case controllers_UserController_user6_route(params@_) =>
      call(params.fromQuery[String]("q", Some(null))) { (q) =>
        controllers_UserController_user6_invoker.call(UserController_4.user(q))
      }
  
    // @LINE:22
    case controllers_UserController_get7_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_UserController_get7_invoker.call(UserController_4.get(id))
      }
  
    // @LINE:24
    case controllers_UserController_login8_route(params@_) =>
      call { 
        controllers_UserController_login8_invoker.call(
          req => UserController_4.login(req))
      }
  
    // @LINE:26
    case controllers_UserController_add9_route(params@_) =>
      call { 
        controllers_UserController_add9_invoker.call(
          req => UserController_4.add(req))
      }
  
    // @LINE:28
    case controllers_UserController_delete10_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_UserController_delete10_invoker.call(UserController_4.delete(id))
      }
  
    // @LINE:31
    case controllers_UserdateController_event11_route(params@_) =>
      call(params.fromQuery[String]("q", Some(null))) { (q) =>
        controllers_UserdateController_event11_invoker.call(UserdateController_1.event(q))
      }
  
    // @LINE:32
    case controllers_UserdateController_get12_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_UserdateController_get12_invoker.call(UserdateController_1.get(id))
      }
  
    // @LINE:34
    case controllers_UserdateController_getUsers13_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_UserdateController_getUsers13_invoker.call(UserdateController_1.getUsers(id))
      }
  
    // @LINE:36
    case controllers_UserdateController_add14_route(params@_) =>
      call { 
        controllers_UserdateController_add14_invoker.call(
          req => UserdateController_1.add(req))
      }
  
    // @LINE:38
    case controllers_UserdateController_update15_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_UserdateController_update15_invoker.call(
          req => UserdateController_1.update(req, id))
      }
  
    // @LINE:40
    case controllers_UserdateController_delete16_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_UserdateController_delete16_invoker.call(UserdateController_1.delete(id))
      }
  
    // @LINE:43
    case controllers_TimeController_time17_route(params@_) =>
      call(params.fromQuery[String]("q", Some(null))) { (q) =>
        controllers_TimeController_time17_invoker.call(TimeController_3.time(q))
      }
  
    // @LINE:44
    case controllers_TimeController_get18_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_TimeController_get18_invoker.call(TimeController_3.get(id))
      }
  
    // @LINE:46
    case controllers_TimeController_add19_route(params@_) =>
      call { 
        controllers_TimeController_add19_invoker.call(
          req => TimeController_3.add(req))
      }
  
    // @LINE:48
    case controllers_TimeController_delete20_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_TimeController_delete20_invoker.call(TimeController_3.delete(id))
      }
  
    // @LINE:51
    case controllers_FrontendController_assetOrDefault21_route(params@_) =>
      call(params.fromPath[String]("file", None)) { (file) =>
        controllers_FrontendController_assetOrDefault21_invoker.call(FrontendController_0.assetOrDefault(file))
      }
  }
}
