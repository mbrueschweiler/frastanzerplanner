name := """infinite-earth-30325"""
organization := "ost"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.3"

libraryDependencies += guice

libraryDependencies ++= Seq(
  evolutions,
  javaJdbc,
  //"com.h2database" % "h2" % "1.4.192",
  "org.postgresql"  % "postgresql"      % "42.2.1",
  javaJpa,
  "org.hibernate" % "hibernate-core" % "5.4.9.Final",
  javaWs
)

libraryDependencies += "org.mockito" % "mockito-core" % "2.10.0" % "test"

herokuAppName in Compile := "infinite-earth-30325"
herokuJdkVersion in Compile := "11"