package repository;

import javax.inject.Inject;
import play.db.jpa.JPAApi;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;
import models.Date;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class DateRepository {
    private final JPAApi jpaApi;

    @Inject
    public DateRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public CompletionStage<Date> add(Date date) {
        return supplyAsync(() -> wrap(em -> insert(em, date)));
    }

    public CompletionStage<Date> update(Date date) {
        return supplyAsync(() -> wrap(em -> update(em, date)));
    }

    public CompletionStage<Stream<Date>> list() {
        return supplyAsync(() -> wrap(this::list));
    }

    public CompletionStage<Date> find(Long id) {
        return supplyAsync(() -> wrap(em -> find(em, id)));
    }

    public CompletionStage<Boolean> delete(Long id) {
        return supplyAsync(() -> wrap(em -> delete(em, id)));
    }

    private <D> D wrap(Function<EntityManager, D> function) {
        return jpaApi.withTransaction(function);
    }

    //Methods

    /**
     * Inserts the given date
     *
     * @param date to insert
     * @return inserted date
     */
    private Date insert(EntityManager em, Date date) {
        System.out.println(date);
        em.persist(date);
        return date;
    }


    /**
     * Updates the given date
     *
     * @param date with new parameters
     * @return updatedDate
     */
    private Date update(EntityManager em, Date date) {
        Date dateToUpdate = em.find(Date.class, date.getId());
        dateToUpdate.setTitle(date.getTitle());
        dateToUpdate.setPlace(date.getPlace());
        dateToUpdate.setDescription(date.getDescription());
        dateToUpdate.setNrpeople(date.getNrpeople());
        dateToUpdate.setTime(date.getTime());
        return dateToUpdate;
    }

    /**
     * Return's a list of all dates
     *
     * @return list of all dates
     */
    //ned sicher ob des stimmt
    private Stream<Date> list(EntityManager em) {
        List<Date> dates = em.createQuery("select d from date d", Date.class).getResultList();
        return dates.stream();
    }

    /**
     * Returns date with given identifier
     * @param id date identifier
     * @return date
     */
    private Date find(EntityManager em, Long id) {
        return em.find(Date.class, id);
    }

    /**
     * Deletes dates with given identifier.
     * @param id dates identifier
     * @return {@code true} on success  {@code false} on failure
     */
    private Boolean delete(EntityManager em, Long id) {
        Date date = em.find(Date.class, id);
        if (null != date) {
            em.remove(date);
            return true;
        } else {
            return false;
        }
    }
}

