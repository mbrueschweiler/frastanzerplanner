package repository;

import javax.inject.Inject;
import play.db.jpa.JPAApi;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;
import models.User;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class UserRepository {
    private final JPAApi jpaApi;

    @Inject
    public UserRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public CompletionStage<User> add(User user) {
        return supplyAsync(() -> wrap(em -> insert(em, user)));
    }

    public CompletionStage<User> update(User user) {
        return supplyAsync(() -> wrap(em -> update(em, user)));
    }

    public CompletionStage<Stream<User>> list() {
        return supplyAsync(() -> wrap(this::list));
    }

    public CompletionStage<User> find(Long id) {
        return supplyAsync(() -> wrap(em -> find(em, id)));
    }

    public CompletionStage<User> findLogin(String mail) {
        return supplyAsync(() -> wrap(em -> findLogin(em, mail)));
    }

    public CompletionStage<Boolean> delete(Long id) {
        return supplyAsync(() -> wrap(em -> delete(em, id)));
    }

    private <D> D wrap(Function<EntityManager, D> function) {
        return jpaApi.withTransaction(function);
    }

    //Methods

    /**
     * Inserts the given user
     *
     * @param user to insert
     * @return inserted user
     */
    private User insert(EntityManager em, User user) {
        em.persist(user);
        return user;
    }


    /**
     * Updates the given user
     *
     * @param user with new parameters
     * @return updatedUser
     */
    private User update(EntityManager em, User user) {
        User userToUpdate = em.find(User.class, user.getId());
        userToUpdate.setMail(user.getMail());
        userToUpdate.setFirstname(user.getFirstname());
        userToUpdate.setName(user.getName());
        userToUpdate.setPassword(user.getPassword());
        userToUpdate.setRole(user.getRole());
        return userToUpdate;
    }

    /**
     * Return's a list of all users
     *
     * @return list of all users
     */
    private Stream<User> list(EntityManager em) {
        List<User> users = em.createQuery("select u from users u", User.class).getResultList();
        return users.stream();
    }

    /**
     * Returns user with given identifier
     * @param id user identifier
     * @return user
     */
    private User find(EntityManager em, Long id) {
        return em.find(User.class, id);
    }

    /**
     * Returns user with given mail
     * @param mail user mail
     * @return user
     */
    private User findLogin(EntityManager em, String mail) {
        return em.find(User.class, mail);
    }

    /**
     * Deletes user with given identifier.
     * @param id user identifier
     * @return {@code true} on success  {@code false} on failure
     */
    private Boolean delete(EntityManager em, Long id) {
        User user = em.find(User.class, id);
        if (null != user) {
            em.remove(user);
            return true;
        } else {
            return false;
        }
    }
}
