package repository;

import javax.inject.Inject;

import models.Date;
import play.db.jpa.JPAApi;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;
import models.Userdate;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class UserdateRepository {
    private final JPAApi jpaApi;

    @Inject
    public UserdateRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public CompletionStage<Userdate> add(Userdate userdate) {
        return supplyAsync(() -> wrap(em -> insert(em, userdate)));
    }

    public CompletionStage<Userdate> update(Userdate userdate) {
        return supplyAsync(() -> wrap(em -> update(em, userdate)));
    }

    public CompletionStage<Stream<Userdate>> list() {
        return supplyAsync(() -> wrap(this::list));
    }

    public CompletionStage<Stream<Userdate>> getUsers(Long id) {
        return supplyAsync(() -> wrap(em -> getUsers(id, em)));
    }

    public CompletionStage<Userdate> find(Long id) {
        return supplyAsync(() -> wrap(em -> find(em, id)));
    }

    public CompletionStage<Boolean> delete(Long id) {
        return supplyAsync(() -> wrap(em -> delete(em, id)));
    }

    private <D> D wrap(Function<EntityManager, D> function) {
        return jpaApi.withTransaction(function);
    }

    //Methods

    /**
     * Inserts the given userdate
     *
     * @param userdate to insert
     * @return inserted userdate
     */
    private Userdate insert(EntityManager em, Userdate userdate) {
        em.persist(userdate);
        return userdate;
    }

    /**
     * Updates the given userdate
     *
     * @param userdate with new parameters
     * @return updatedUserate
     */
    private Userdate update(EntityManager em, Userdate userdate) {
        Userdate userdateToUpdate = em.find(Userdate.class, userdate.getId());
        userdateToUpdate.setUser_id(userdate.getUser_id());
        userdateToUpdate.setDate_id(userdate.getDate_id());
        userdateToUpdate.setFixed(userdate.isFixed());
        return userdateToUpdate;
    }

    /**
     * Return's a list of all Userdates
     *
     * @return list of all Userdates
     */
    private Stream<Userdate> list(EntityManager em) {
        List<Userdate> userdates = em.createQuery("select u from userdate u", Userdate.class).getResultList();
        return userdates.stream();
    }

    /**
     * Return's a list of all Userdates with a given date_id
     * @param id date identifier
     * @return list of all Userdates with a given date_id
     */
    private Stream<Userdate> getUsers(Long id, EntityManager em){
        List<Userdate> userdates = em.createQuery("select u from userdate u where u.date_id = :date", Userdate.class)
                .setParameter("date", id)
                .getResultList();
        return userdates.stream();
    }

    /**
     * Returns userdate with given identifier
     * @param id userdate identifier
     * @return usesrdate
     */
    private Userdate find(EntityManager em, Long id) {
        return em.find(Userdate.class, id);
    }

    /**
     * Deletes userdates with given identifier.
     * @param id userdates identifier
     * @return {@code true} on success  {@code false} on failure
     */
    private Boolean delete(EntityManager em, Long id) {
        Userdate userdate = em.find(Userdate.class, id);
        if (null != userdate) {
            em.remove(userdate);
            return true;
        } else {
            return false;
        }
    }
}

