package services;

import models.Date;
import repository.DateRepository;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;


public class DefaultDateService implements DateService{

    private DateRepository dateRepository;

    @Inject
    public DefaultDateService(DateRepository dateRepository){
        this.dateRepository = dateRepository;
    }

    /**
     * Return's list of all dates.
     * @return list of all dates
     */
    public CompletionStage<Stream<Date>> get() {
        return dateRepository.list();
    }

    /**
     * Returns date with given identifier.
     * @param id date identifier
     * @return date with given identifier or {@code null}
     */
    public CompletionStage<Date> get(Long id) {
        return dateRepository.find(id);
    }

    /**
     * Removes date with given identifier.
     * @param id date identifier
     * @return {@code true} on success  {@code false} on failure
     */
    public CompletionStage<Boolean> delete(Long id) {
        return dateRepository.delete(id);
    }

    /**
     * Updates date with given identifier.
     * @param date date with updated fields
     * @return updated date
     */
    public CompletionStage<Date> update(Date date) {
        return dateRepository.update(date);
    }

    /**
     * Adds the given date.
     * @param date to add
     * @return added date
     */
    public CompletionStage<Date> add(Date date) {
        return dateRepository.add(date);
    }
}
