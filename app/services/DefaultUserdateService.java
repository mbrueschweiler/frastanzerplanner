package services;

import models.Date;
import models.Userdate;
import repository.UserdateRepository;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;


public class DefaultUserdateService implements UserdateService{

    private UserdateRepository userdateRepository;

    @Inject
    public DefaultUserdateService(UserdateRepository userdateRepository){
        this.userdateRepository = userdateRepository;
    }

    /**
     * Return's list of all userdates.
     * @return list of all userdates
     */
    public CompletionStage<Stream<Userdate>> get() {
        return userdateRepository.list();
    }

    /**
     * Return's list of all userdates with given date_id
     * @param id date identifier
     * @return list of all userdates
     */
    public CompletionStage<Stream<Userdate>> getUsers(Long id) {
        return userdateRepository.getUsers(id);
    }

    /**
     * Returns userdate with given identifier.
     * @param id userdate identifier
     * @return userdate with given identifier or {@code null}
     */
    public CompletionStage<Userdate> get(Long id) {
        return userdateRepository.find(id);
    }

    /**
     * Removes userdate with given identifier.
     * @param id userdate identifier
     * @return {@code true} on success  {@code false} on failure
     */
    public CompletionStage<Boolean> delete(Long id) {
        return userdateRepository.delete(id);
    }

    /**
     * Updates userdate with given identifier.
     * @param userdate date with updated fields
     * @return updated userdate
     */
    public CompletionStage<Userdate> update(Userdate userdate) {
        return userdateRepository.update(userdate);
    }

    /**
     * Adds the given userdate.
     * @param userdate to add
     * @return added userdate
     */
    public CompletionStage<Userdate> add(Userdate userdate) {
        return userdateRepository.add(userdate);
    }
}
