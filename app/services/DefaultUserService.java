package services;

import models.User;
import repository.UserRepository;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class DefaultUserService implements UserService {

    private UserRepository userRepository;

    @Inject
    public DefaultUserService(UserRepository userRepository){this.userRepository = userRepository;}

    /**
     * Return's list of all users.
     * @return list of all users
     */
    public CompletionStage<Stream<User>> get() {
        return userRepository.list();
    }

    /**
     * Returns user with given identifier.
     * @param id user identifier
     * @return user with given identifier or {@code null}
     */
    public CompletionStage<User> get(Long id) {
        return userRepository.find(id);
    }

    /**
     * Returns user with given mailaddress.
     * @param mail user mail
     * @return user with given mail or {@code null}
     */
    public CompletionStage<User> login(String mail) {
        return userRepository.findLogin(mail);
    }

    /**
     * Removes user with given identifier.
     * @param id user identifier
     * @return {@code true} on success  {@code false} on failure
     */
    public CompletionStage<Boolean> delete(Long id) {
        return userRepository.delete(id);
    }

    /**
     * Updates user with given identifier.
     * @param user with updated fields
     * @return updated user
     */
    public CompletionStage<User> update(User user) {
        return userRepository.update(user);
    }

    /**
     * Adds the given user.
     * @param user to add
     * @return added user
     */
    public CompletionStage<User> add(User user) {
        return userRepository.add(user);
    }

}
