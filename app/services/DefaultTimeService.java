package services;

import models.Time;
import repository.TimeRepository;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class DefaultTimeService implements TimeService{

    private TimeRepository timeRepository;

    @Inject
    public DefaultTimeService(TimeRepository timeRepository){
        this.timeRepository = timeRepository;
    }

    /**
     * Return's list of all times.
     * @return list of all times
     */
    public CompletionStage<Stream<Time>> get() {
        return timeRepository.list();
    }

    /**
     * Returns time with given identifier.
     * @param id time identifier
     * @return time with given identifier or {@code null}
     */
    public CompletionStage<Time> get(Long id) {
        return timeRepository.find(id);
    }

    /**
     * Removes time with given identifier.
     * @param id time identifier
     * @return {@code true} on success  {@code false} on failure
     */
    public CompletionStage<Boolean> delete(Long id) {
        return timeRepository.delete(id);
    }

    /**
     * Adds the given time.
     * @param time to add
     * @return added time
     */
    public CompletionStage<Time> add(Time time) {
        return timeRepository.add(time);
    }
}
