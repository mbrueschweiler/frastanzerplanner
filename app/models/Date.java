package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name= "date")
public class Date {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //Datafields
    private long id;
    private String date;
    private String title;
    private String place;
    private String description;
    private long nrpeople;


    //Methods
    public long getId(){return id;}
    public void setId(Long id) { this.id = id; }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public String getPlace() { return place; }
    public void setPlace(String place) { this.place = place; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public long getNrpeople(){return nrpeople;}
    public void setNrpeople(Long nrpeople) { this.nrpeople= nrpeople; }

    public String getTime() { return date; }
    public void setTime(String date){ this.date = date; }
}
