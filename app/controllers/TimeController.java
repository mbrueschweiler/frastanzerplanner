package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Time;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.TimeService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class TimeController extends Controller {

    private final TimeService timeService;

    @Inject
    public TimeController(TimeService timeService) {
        this.timeService = timeService;
    }

    /**
     * Request to get all times.
     * @param q string
     * @return request to TimeService to get all times.
     */
    public CompletionStage<Result> time(String q) {
        return timeService.get().thenApplyAsync(timeStream -> ok(Json.toJson(timeStream.collect(Collectors.toList()))));
    }

    /**
     * Request to get a time with a given id.
     * @param  id long
     * @return request to TimeService to get a specific time.
     */
    public CompletionStage<Result> get(long id) {
        return timeService.get(id).thenApplyAsync(time -> ok(Json.toJson(time)));
    }

    /**
     * Request to add a time
     * @param  request http.Request
     * @return request to TimeService to post a new time.
     */
    public CompletionStage<Result> add(Http.Request request) {
        JsonNode jsonTime = request.body().asJson();
        Time newTime = Json.fromJson(jsonTime, Time.class);
        return timeService.add(newTime).thenApplyAsync(time -> ok(Json.toJson(time)));
    }

    /**
     * Request to delet a time with a given id
     * @param  id long
     * @return request to TimeService to delete a specific time.
     */
    public CompletionStage<Result> delete(long id) {
        return timeService.delete(id).thenApplyAsync(deleted -> deleted ? ok() : internalServerError());
    }
}
