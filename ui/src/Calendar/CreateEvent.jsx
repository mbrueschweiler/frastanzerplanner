import React from "react";
import "../GlobalStyles.css";
import {withRouter} from 'react-router-dom';
import {toast} from 'react-toastify';

class CreateEvent extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            date: "",
            title: "",
            place: "",
            nrpeople: 1,
            description: "",
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.back = this.back.bind(this);
    }

    //Save Event to DB
    handleSubmit(event){
        event.preventDefault();
        //Create JSON
        let body = {
            date: this.state.date,
            title: this.state.title,
            place: this.state.place,
            nrpeople: parseInt(this.state.nrPeople),
            description: this.state.description
        };
                //console.log(JSON.stringify(body));
        //validation
            if(body.date === 0 || body.title.length === 0 || body.place.length === 0 || body.nrpeople.length === 0){
                toast.warn("Alle Felder sind auszufüllen", {position: "bottom-center",});
            }
            else {
                //Post Data on success
                    //console.log("success");
                fetch('/api/events', {
                    method: 'post',
                    body: JSON.stringify(body),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }

                }).then(response => {
                    //response success / error
                    if (response.status !== 200) {
                        toast.error('Ups! Da ist wohl ein Fehler passiert', {
                            position: "bottom-center",
                        });
                    } else {
                        toast('Event hinzugefügt', {
                            position: "bottom-center",
                        });
                        // set initial state
                        this.setState({
                            date: "",
                            title: "",
                            place: "",
                            nrPeople: 1,
                            description: ""
                        });
                        return response.json();
                    }
                })
            }
    }

    //Update state OnChange
    handleInputChange(event){
        let target = event.target;
        let value = target.value;
        let name = target.name;
        //console.log(value);
        //console.log(name);

        this.setState({[name]: value});
    }

    //Link back to Calendar-page
    back(){
        let path = "/calendar";
        this.props.history.push(path);
    }


    render(){
        return(
            <section>
                <button className="margin button" onClick={this.back}>Zurück zum Kalender</button>
                <form className="input-form" id="form" onSubmit={this.handleSubmit}>
                    <h1>Neues Event</h1>
                    <div className="frame">
                    <label>Titel:</label>
                    <input className="margin2" type="text" name="title" value={this.state.title} onChange={this.handleInputChange}/><br/>

                    <label>Ort:</label>
                    <input className="margin2" type="text" name="place" value={this.state.place} onChange={this.handleInputChange}/><br/>

                    <label>Datum:</label>
                    <input className="margin2" type="datetime-local" name="date" value={this.state.date} onChange={this.handleInputChange}/><br/>

                    <label>Anzahl Personen:</label>
                    <input className="margin2" type="number" name="nrPeople" value={parseInt(this.state.nrPeople)} min="1" max="20" onChange={this.handleInputChange}/><br/>

                    <label>Beschreibung:</label><br/>
                        <textarea form="form" name="description" value={this.state.description} onChange={this.handleInputChange}/><br/>

                        <input className="submit button margin" type="submit" value="Event hinzufügen"/>
                    </div>
                </form>
            </section>
        )
    }
}
export default withRouter(CreateEvent);