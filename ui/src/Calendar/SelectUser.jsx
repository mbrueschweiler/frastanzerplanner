import React from "react";
import "../GlobalStyles.css";
import {toast} from "react-toastify";

class SelectUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value : 0,
            date_id : this.props.dateid,
            title : this.props.title,
            users : [],
            isApplied: true,
        };
        this.handleChange = this.handleChange.bind(this);
        this.filterUsers = this.filterUsers.bind(this);
        this.apply = this.apply.bind(this);
        this.checkApplication = this.checkApplication.bind(this);
    }

    componentDidMount() {
        //get Users
        fetch('/api/users')
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    users: this.filterUsers(data)
                });
            });
    }

    //filters users
    //Administratoren werden herausgefiltert
    filterUsers(data){
        let users = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].role === false) {
                users.push(data[i])
            }
        }
        return users;
    }

    //Save Input Change in this.state
    handleChange(event){
        this.setState({value: event.target.value});
        this.checkApplication( event.target.value,this.state.date_id);
    }

    optionUser(user){
        return(
            <option value={user.id}>{user.firstname} {user.name}</option>
        )
    }

    //Apply the selected user for the Event
    apply(){
        //Create JSON
        let body = {
            user_id: this.state.value,
            date_id: this.state.date_id,
            fixed: false
        };
        //validation: user selected?
        if(body.user_id === 0){
            toast.warn('Bitte einen Benutzer wählen', {
                position: "bottom-center",
            });
        }
        else{
            //validation: user already applied?
            if(this.state.isApplied){
                toast.error('Benutzer ist bereits angemeldet', {
                    position: "bottom-center",
                });
            }
            else{
                //Post Data on success
                    //console.log("success");
                fetch('/api/applications', {
                    method: 'post',
                    body: JSON.stringify(body),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }

                }).then(response => {
                    //response success / error
                    if (response.status !== 200) {
                        toast.error('Ups! Da ist wohl ein Fehler passiert', {
                            position: "bottom-center",
                        });
                    } else {
                        toast('Benutzer für ' + this.state.title + ' angemeldet!', {
                            position: "bottom-center",
                        });
                        return response.json();
                    }
                })
            }
        }
    }

    //returns true if user has already applied
    checkApplication(user_id, date_id){
        //console.log("check");
        //get all applied users for the given event
        fetch('/api/applications/event/'+ date_id)
            .then(res => {
                return res.json();
            })
            .then(data => {
               //check if user exists in the list
                let matchfound = false;
                for(let k = 0; k < data.length; k++) {
                    //console.log("enter for");
                    //console.log(data[k].user_id);
                    //console.log(user_id);
                    // eslint-disable-next-line
                    if (data[k].user_id == user_id) {
                        //console.log("user match found");
                        matchfound = true;
                    }
                }
                this.setState({isApplied: matchfound});
            });
    }

    render() {
        //console.log(this.state.date_id);
        return(
            <div>
            <select value={this.state.value} onChange={this.handleChange}>
                <option>Benutzer wählen</option>
                {
                    this.state.users.length > 0 ?
                        this.state.users.map(this.optionUser) : <option>Keine User verfügbar</option>
                }
            </select>
            <button className="button" onClick={() => this.apply()} >Anmelden</button>
            </div>
        )
    }
}
export default SelectUser;