import React from "react";
import "../GlobalStyles.css";
import {withRouter} from 'react-router-dom'
import Event from "./Event";

class Calendar extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dates: []
        };
        this.createDate = this.createDate.bind(this);
        this.viewApplications = this.viewApplications.bind(this);
    }

    //Get Dates from database
    componentDidMount(){
        fetch('/api/events ')
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    dates: data
                });
            });
    }

    //Change path to createdate
    createDate(){
        let path = "/create/event";
        this.props.history.push(path);
    }

    //Change path to applications
    viewApplications(){
        let path = "/applications/show";
        this.props.history.push(path);
    }


    render() {
        return (
            <section>
                <h1>Kalender</h1>
                <button className="button3" onClick={this.createDate}>Termin hinzufügen</button>
                <button className="button3" onClick={this.viewApplications}>Anmeldungen ansehen & fixieren</button>
                {
                    this.state.dates.length > 0 ?
                        this.state.dates.map(function(date) {
                            return (
                                <Event key={date.id} date={date} />
                            );
                        }) : <h1>No Events</h1>
                }
            </section>
        )
    }
}

export default withRouter(Calendar);