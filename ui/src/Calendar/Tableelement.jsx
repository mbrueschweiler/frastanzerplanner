import React from "react";
import "../GlobalStyles.css";
//import {Table} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from '@material-ui/core';
import {toast} from "react-toastify";

class Tableelement extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            event_id : this.props.event.id,
            user_id: this.props.user,
            applications: [],
            disabledButton: false,
            checked: true,
        };
        this.fixUser = this.fixUser.bind(this);
    }

    componentDidMount() {
        fetch('/api/applications/event/'+ this.state.event_id)
            .then(res => {
                return res.json();
            })
            .then(data => {
                //console.log(data);
                this.setState({applications: data});
            });
    }
    //Update application to 'fixed = true'
    fixUser(fixdate){
        //console.log('fixieren');
        //Create Json
        let body = {
            id: fixdate.id,
            user_id: fixdate.user_id,
            date_id: fixdate.date_id,
            fixed: true
        };
        //put json
        fetch('/api/applications/'+fixdate.id, {
            method: 'put',
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).then(response => {
            //response success / error
            if (response.status !== 200) {
                toast.error('Ups! Da ist wohl ein Fehler passiert', {
                    position: "bottom-center",
                });
            } else {
                this.setState({disabledButton: true});
                toast('Teilnehmer fixiert!', {
                    position: "bottom-center",
                });
                //this.props.history.push('/calendar');
                return response.json();
            }
        })

    }

    render(){
        const items = [];

        for (let k = 0; k < this.state.applications.length; k++) {
            if ( this.state.applications[k].user_id === this.state.user_id) {
                if(this.state.applications[k].fixed === true){
                    //console.log("checked");
                    items.push(<input type="checkbox" checked={this.state.checked} disabled={this.state.checked}/>);
                }
                else{
                    items.push(<div><input type="checkbox"  checked={this.state.checked} disabled={this.state.checked}/><Button variant="outlined" disabled={this.state.disabledButton} className='button2 margin' onClick={() => this.fixUser(this.state.applications[k])}>Fixieren</Button></div>);
                }
                break;
            }
        }
        if(items.length === 0){
            items.push(<input type="checkbox"  disabled={this.state.checked}/>);
        }

        return(
                 <div> {items}</div>
                );
    }

}
export default Tableelement;