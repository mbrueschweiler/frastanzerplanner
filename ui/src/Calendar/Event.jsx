import React from "react";
import "./EventStyles.css";
import {toast} from 'react-toastify';
import SelectUser from "./SelectUser";

class Event extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            date_id : this.props.date.id,
            title: this.props.date.title,
        }
        this.deleteDate = this.deleteDate.bind(this);

    }

    //delete event with given id
    deleteDate(id){
        //console.log(id);
        fetch('/api/events/' + id, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            //response success / error
            if (response.status !== 200) {
                toast.error('Ups! Da ist wohl ein Fehler passiert', {
                    position: "bottom-center",
                });
            } else {
                toast('Event gelöscht', {
                    position: "bottom-center",
                });
            }
        })
    }


    render(){
        return(
            <section>
                <h1>{this.state.title}</h1>
                <div className="date">
                    <p>Datum: {this.props.date.date}</p>
                    <p>Benötigte Personen: {this.props.date.nrpeople}</p>
                    <SelectUser key={this.state.date_id} dateid={this.state.date_id} title={this.state.title}/>
                    <button onClick={() => this.deleteDate(this.props.date.id)} className="button">Termin löschen</button>
                </div>
            </section>
        )
    }

}
export default Event;