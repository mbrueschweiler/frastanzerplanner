import React from 'react';
//import logo from './logo.svg';
import './App.css';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Calendar from "./Calendar/Calendar";
import Home from "./Home"
import CreateEvent from "./Calendar/CreateEvent";
import Createuser from "./Createuser";
import ApplicationsShow from "./Calendar/ApplicationsShow";
import Applications from "./Application/Applications";

import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Navbar, Nav} from 'react-bootstrap';

export default function App() {
    
  return (
    <div className="App">
        <ToastContainer/>
        <Router>
            <header className="App-header">
                <Navbar className="App-header" expand="lg">
                <Navbar.Brand href="/">Frastanzer Planner</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/calendar">Kalender</Nav.Link>
                    <Nav.Link href="/applications">Einsätze</Nav.Link>
                    <Nav.Link href="/time">Stundenübersicht</Nav.Link>
                    <Nav.Link href="/create/user">User hinzufügen</Nav.Link>
                </Nav>
                </Navbar.Collapse>
                </Navbar>
            </header>

            <Switch>
                <Route path="/calendar"><Calendar/></Route>
                <Route path="/applications/show"><ApplicationsShow/></Route>
                <Route path="/create/event"><CreateEvent/></Route>
                <Route path="/create/user"><Createuser/></Route>
                <Route path="/applications/:id"></Route>
                <Route path="/applications"><Applications/></Route>
                <Route path="/time/:id"></Route>
                <Route path="/"><Home/></Route>
            </Switch>
        </Router>
    </div>
  );
}

