import React from "react";
import "../GlobalStyles.css";
import {withRouter} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Einsatz from "../Einsatz";


class Applications extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
            dates: [],
        };
    }

    componentDidMount() {
        fetch('/api/events')
        .then(res => {
            return res.json();
        })
        .then(data => {
            this.setState({
                dates: data,
            });
        });
    }

    render() {
        return (
            <section>
                {
                    this.state.dates.length > 0 ?
                        this.state.dates.map(function(date){
                            return( <Einsatz key={date.id} date={date}/>)
                         }):<h1>No Events Available!</h1>
                }

            </section>
        );
    }
}
export default withRouter(Applications);