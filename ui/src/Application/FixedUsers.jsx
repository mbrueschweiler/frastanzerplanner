import React from "react";
import "../GlobalStyles.css";
import {withRouter} from 'react-router-dom';
import GetUsername from "./GetUsername";

class FixedUsers extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            applications: [],
        };
         this.filterApplications = this.filterApplications.bind(this);
    }

    componentDidMount() {
        fetch('/api/applications/event/'+ this.props.date.id)
        .then(res => {
            return res.json();
        })
        .then(data => {
        //console.log(data);
            this.setState({
                applications: this.filterApplications(data)
            });
        });
    }

    filterApplications(data){
        let fixedApplications = [];
        for(let i = 0; i < data.length; i++){
            if(data[i].fixed === true)
                fixedApplications.push(data[i]);
        }
        return fixedApplications;
    }

    render() {
        return (
            <section>
                {
                    this.state.applications.length > 0 ?
                        this.state.applications.map(function(application){
                            return(
                                <GetUsername key={application.id} application={application}/>
                            )
                        }) : <p>Noch niemand fixiert</p>
                }
            </section>
        );
    }
}
export default withRouter(FixedUsers);