import React from "react";
import "../GlobalStyles.css";

class GetUsername extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: this.props.application.user_id,
            username: "",
        };

    }

    componentDidMount() {
        fetch(' /api/users/'+this.state.user_id)
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    username: data.firstname+" "+data.name
                });
            });
    }

    render() {
        return(
            <p>{this.state.username}</p>
        )
    }
}
export default GetUsername;